package com.lucaspasquier.desafioconcrete;

import com.lucaspasquier.desafioconcrete.interactor.RepositoryInteractor;
import com.lucaspasquier.desafioconcrete.model.Repository;
import com.lucaspasquier.desafioconcrete.model.api.APiRepository;
import com.lucaspasquier.desafioconcrete.service.Api;

import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import rx.observers.TestSubscriber;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by lucaspasquier on 31/10/17.
 */

public class RepositoryInteractorTest {

    @Mock
    Api api;

    @Mock
    RepositoryInteractor interactor;

    @Test
    public void testGetRepositories() {
        TestSubscriber<List<Repository>> testSubscriber = TestSubscriber.create();
        interactor.getRepositories("Java","stars", 0).subscribe(testSubscriber);

        //verify(api).getRepositories(eq("Java","stars", 0));
    }
}
