package com.lucaspasquier.desafioconcrete.di.component.common;

import com.google.gson.Gson;
import com.lucaspasquier.desafioconcrete.App;
import com.lucaspasquier.desafioconcrete.di.module.common.ApiModule;
import com.lucaspasquier.desafioconcrete.di.module.common.AppModule;
import com.lucaspasquier.desafioconcrete.di.module.common.InteractorModule;
import com.lucaspasquier.desafioconcrete.interactor.RepositoryInteractor;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by lucaspasquier on 29/10/17.
 */

@Singleton
@Component(
        modules = {
                AppModule.class,
                ApiModule.class,
                InteractorModule.class
        }
)
public interface AppComponent {
    void inject(App app);
    Gson gson();
    RepositoryInteractor provideRepositoryInteractor();
}
