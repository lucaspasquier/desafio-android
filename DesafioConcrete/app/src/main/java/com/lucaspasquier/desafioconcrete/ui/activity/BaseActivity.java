package com.lucaspasquier.desafioconcrete.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.lucaspasquier.desafioconcrete.App;
import com.lucaspasquier.desafioconcrete.di.component.common.AppComponent;
import com.lucaspasquier.desafioconcrete.ui.dialog.ProgressDialog;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent(App.getComponent());
    }

    protected abstract void setupComponent(AppComponent appComponent);

    protected void showProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            hideProgress();
        }

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.show();
    }

    protected void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}
