package com.lucaspasquier.desafioconcrete.model;

import com.lucaspasquier.desafioconcrete.model.api.ApiOwner;

import java.io.Serializable;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class Owner implements Serializable {

    private String login;
    private int id;
    private String avatarUrl;

    public Owner() {
        this.id = 0;
        this.login = "";
        this.avatarUrl = "";
    }

    public Owner(ApiOwner owner) {
        this.id = owner.getId();
        this.login = owner.getLogin();
        this.avatarUrl = owner.getAvatarUrl();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
