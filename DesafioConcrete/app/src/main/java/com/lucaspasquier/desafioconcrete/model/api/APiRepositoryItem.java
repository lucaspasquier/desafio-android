package com.lucaspasquier.desafioconcrete.model.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class APiRepositoryItem implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("owner")
    private ApiOwner owner;

    @SerializedName("description")
    private String description;

    @SerializedName("forks_count")
    private int forksCount;

    @SerializedName("stargazers_count")
    private int stargazersNount;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public ApiOwner getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public int getForksCount() {
        return forksCount;
    }

    public int getStargazersNount() {
        return stargazersNount;
    }
}
