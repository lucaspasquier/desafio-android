package com.lucaspasquier.desafioconcrete.ui.view;

import com.lucaspasquier.desafioconcrete.model.Repository;

import java.util.List;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public interface RepositoryView {
    void showProgress();
    
    void hideProgress();
    
    void loadErrorScreen();

    void loadResult(List<Repository> repositories);

    void loadPagination(List<Repository> repositories);

    void loadEmptyResult();

    void scrollPosition(int position);
}
