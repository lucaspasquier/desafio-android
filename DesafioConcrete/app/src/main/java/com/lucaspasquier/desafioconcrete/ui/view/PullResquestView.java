package com.lucaspasquier.desafioconcrete.ui.view;

import com.lucaspasquier.desafioconcrete.model.PullRequest;
import com.lucaspasquier.desafioconcrete.model.Repository;

import java.util.List;

/**
 * Created by lucaspasquier on 30/10/17.
 */

public interface PullResquestView {
    void showProgress();

    void hideProgress();

    void loadErrorScreen();

    void loadResult(List<PullRequest> pullRequests);

    void loadEmptyResult();

    void loadRepositoryInfo(Repository reposiroty);

    void scrollToPosition(int position);
}
