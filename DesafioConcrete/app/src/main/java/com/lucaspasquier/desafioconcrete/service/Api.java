package com.lucaspasquier.desafioconcrete.service;

import com.lucaspasquier.desafioconcrete.model.api.APiRepository;
import com.lucaspasquier.desafioconcrete.model.api.ApiPullRequest;

import java.util.List;

import rx.Observable;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public interface Api {

    @GET("search/repositories")
    Observable<APiRepository> getRepositories(@Query("q") String language,
                                              @Query("sort") String stars,
                                              @Query("page") int page);

    @GET("repos/{owner}/{repo}/pulls")
    Observable<List<ApiPullRequest>> getPullRequests(@Path("owner") String owner,
                                                    @Path("repo") String repositoryName,
                                                     @Query("state") String state);
}
