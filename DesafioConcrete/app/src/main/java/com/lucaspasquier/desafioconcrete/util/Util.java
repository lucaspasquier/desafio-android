package com.lucaspasquier.desafioconcrete.util;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

public class Util {

    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static int getPosition(String text, char c) {
        try {
            return text.indexOf(c) + 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public static String getDateAsString(Date date) {
        //2017-10-30T18:17:25Z
        return new SimpleDateFormat("dd/mm/yyyy HH:mm").format(date);
    }
}
