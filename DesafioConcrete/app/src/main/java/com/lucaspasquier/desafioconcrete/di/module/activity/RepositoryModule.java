package com.lucaspasquier.desafioconcrete.di.module.activity;

import com.lucaspasquier.desafioconcrete.interactor.RepositoryInteractor;
import com.lucaspasquier.desafioconcrete.presenter.RepositoryPresenter;
import com.lucaspasquier.desafioconcrete.presenter.SplashPresenter;
import com.lucaspasquier.desafioconcrete.presenter.impl.RepositoryPresenterImpl;
import com.lucaspasquier.desafioconcrete.presenter.impl.SplashPresenterImpl;
import com.lucaspasquier.desafioconcrete.ui.view.RepositoryView;
import com.lucaspasquier.desafioconcrete.ui.view.SplashView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lucaspasquier on 29/10/17.
 */

@Module
public class RepositoryModule {

    private RepositoryView view;

    public RepositoryModule(RepositoryView view) {
        this.view = view;
    }

    @Provides
    public RepositoryView provideRepositoryView() {
        return this.view;
    }

    @Provides
    public RepositoryPresenter provideRepositoryPresenter(RepositoryView view, RepositoryInteractor interactor) {
        return new RepositoryPresenterImpl(view, interactor);
    }
}
