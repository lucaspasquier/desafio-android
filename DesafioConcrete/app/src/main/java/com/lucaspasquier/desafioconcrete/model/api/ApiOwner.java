package com.lucaspasquier.desafioconcrete.model.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class ApiOwner implements Serializable {

    @SerializedName("login")
    private String login;

    @SerializedName("id")
    private int id;

    @SerializedName("avatar_url")
    private String avatarUrl;

    public String getLogin() {
        return login;
    }

    public int getId() {
        return id;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

}

