package com.lucaspasquier.desafioconcrete.model;

import com.google.gson.annotations.SerializedName;
import com.lucaspasquier.desafioconcrete.model.api.ApiOwner;
import com.lucaspasquier.desafioconcrete.service.Api;

import java.io.Serializable;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class Repository implements Serializable {

    public static final String ID_OBJECT = "com.lucaspasquier.desafioconcrete.model.Repository";

    private int id;

    private String name;

    private String fullname;

    private Owner owner;

    private String description;

    private int forksCount;

    private int stargazersCount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name == null) {
            name = "";
        }

        this.name = name;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(ApiOwner owner) {
        if(owner == null) {
            this.owner = new Owner();
        } else {
            this.owner = new Owner(owner);
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if(description == null) {
            description = "";
        }

        this.description = description;
    }

    public int getForksCount() {
        return forksCount;
    }

    public void setForksCount(int forksCount) {
        this.forksCount = forksCount;
    }

    public int getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(int stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        if(fullname == null) {
            fullname = "";
        }

        this.fullname = fullname;
    }
}
