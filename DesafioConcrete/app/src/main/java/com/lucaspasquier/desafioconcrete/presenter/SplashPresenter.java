package com.lucaspasquier.desafioconcrete.presenter;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public interface SplashPresenter {

    void onCreate();
    void onDestroy();
}
