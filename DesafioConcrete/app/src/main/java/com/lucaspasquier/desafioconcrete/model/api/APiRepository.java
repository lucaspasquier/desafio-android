package com.lucaspasquier.desafioconcrete.model.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class APiRepository implements Serializable {

    @SerializedName("total_count")
    private int totalCount;

    @SerializedName("incomplete_results")
    private boolean incompleteResults;

    @SerializedName("items")
    private List<APiRepositoryItem> items;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public boolean getIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public List<APiRepositoryItem> getItems() {
        return items;
    }

    public void setItems(List<APiRepositoryItem> items) {
        this.items = items;
    }
}
