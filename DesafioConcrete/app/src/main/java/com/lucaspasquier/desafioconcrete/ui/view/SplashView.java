package com.lucaspasquier.desafioconcrete.ui.view;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public interface SplashView {
    void navigateToRepositories();

    void setUpVideoBackground();
}
