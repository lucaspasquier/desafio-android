package com.lucaspasquier.desafioconcrete.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lucaspasquier.desafioconcrete.R;
import com.lucaspasquier.desafioconcrete.di.component.activity.DaggerRepositoryComponent;
import com.lucaspasquier.desafioconcrete.di.component.activity.DaggerSplashComponent;
import com.lucaspasquier.desafioconcrete.di.component.common.AppComponent;
import com.lucaspasquier.desafioconcrete.di.module.activity.RepositoryModule;
import com.lucaspasquier.desafioconcrete.di.module.activity.SplashModule;
import com.lucaspasquier.desafioconcrete.model.Repository;
import com.lucaspasquier.desafioconcrete.presenter.RepositoryPresenter;
import com.lucaspasquier.desafioconcrete.ui.adapter.RepositoryAdapter;
import com.lucaspasquier.desafioconcrete.ui.view.RepositoryView;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import jp.wasabeef.recyclerview.animators.SlideInDownAnimator;

public class RepositoryActivity extends BaseActivity implements RepositoryView {

    private static final String PAGE = "page";
    private static final String SCROLL_POSITION = "SCROLL_POSITION";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rcvRepositories)
    RecyclerView rcvRepositories;

    @BindView(R.id.lltEmptyResult)
    LinearLayout lltEmptyResult;

    @BindView(R.id.txtDescription)
    TextView txtDescription;

    @BindString(R.string.app_name) String title;

    @Inject
    RepositoryPresenter presenter;

    private Unbinder unbinder;
    private RepositoryAdapter adapter;
    private LinearLayoutManager layoutManager;
    private int page;
    private boolean emptyResult;
    private List<Repository> repositories;
    private int scrollPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);
        unbinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(title);

        this.emptyResult = false;

        if (savedInstanceState != null && !savedInstanceState.isEmpty()) {
            presenter.onReceiveSerializable(savedInstanceState.getSerializable(Repository.ID_OBJECT));
            page = savedInstanceState.getInt(PAGE);
            scrollPosition = savedInstanceState.getInt(SCROLL_POSITION);
        }

        presenter.onCreate(page, scrollPosition);
    }

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerRepositoryComponent.builder()
                .appComponent(appComponent)
                .repositoryModule(new RepositoryModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(Repository.ID_OBJECT, (Serializable) repositories);
        outState.putInt(PAGE, page);
        outState.putInt(SCROLL_POSITION, layoutManager.findFirstVisibleItemPosition());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        presenter.onDestroy();
    }

    @Override
    public void showProgress() {
        super.showProgress();
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
    }

    @Override
    public void loadResult(List<Repository> repositories) {
        this.repositories = repositories;
        adapter = new RepositoryAdapter(RepositoryActivity.this, repositories);
        layoutManager = new LinearLayoutManager(RepositoryActivity.this, LinearLayoutManager.VERTICAL, false);

        rcvRepositories.setLayoutManager(layoutManager);
        rcvRepositories.setItemAnimator(new SlideInDownAnimator());
        rcvRepositories.setAdapter(adapter);

        rcvRepositories.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (!emptyResult) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                        presenter.loadMoreItens(++page);
                    }
                }
            }
        });

        adapter.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(RepositoryActivity.this, PullResquestActivity.class);
            intent.putExtra(Repository.ID_OBJECT, repositories.get(position));

            startActivity(intent);
        });
    }

    @Override
    public void loadPagination(List<Repository> repositories) {
        adapter.notifyDataSetChanged(repositories);
    }

    @Override
    public void loadErrorScreen() {
        lltEmptyResult.setVisibility(View.VISIBLE);
        txtDescription.setText(getString(R.string.unexpected_error));
    }

    @Override
    public void loadEmptyResult() {
        this.emptyResult = true;
        lltEmptyResult.setVisibility(View.VISIBLE);
        txtDescription.setText(getString(R.string.empty_result));
    }

    @Override
    public void scrollPosition(int position) {
        rcvRepositories.scrollToPosition(scrollPosition);
    }
}
