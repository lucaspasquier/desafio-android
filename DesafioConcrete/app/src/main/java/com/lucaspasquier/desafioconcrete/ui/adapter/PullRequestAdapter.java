package com.lucaspasquier.desafioconcrete.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.lucaspasquier.desafioconcrete.R;
import com.lucaspasquier.desafioconcrete.model.PullRequest;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.ViewHolder>{

    private final Context context;
    private AdapterView.OnItemClickListener mOnItemClickListener;
    private List<PullRequest> pullRequests;

    public PullRequestAdapter(Context context, List<PullRequest> pullRequests) {
        this.context = context;
        this.pullRequests = pullRequests;
    }


    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(PullRequestAdapter.ViewHolder itemHolder) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClick(null, itemHolder.itemView,
                    itemHolder.getAdapterPosition(), itemHolder.getItemId());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_pull_request, parent, false);
        return new ViewHolder(view, PullRequestAdapter.this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PullRequest pull = pullRequests.get(position);

        holder.txtRepositoryName.setText(pull.getTitle());
        holder.txtRepositoryDescription.setText(pull.getDescription());
        holder.txtLogin.setText(pull.getOwner().getLogin());
        holder.txCreatedAt.setText(String.format(context.getString(R.string.pull_dt_created), pull.getCreatedAtToString()));

        if(!pull.getOwner().getAvatarUrl().isEmpty()) {
            Glide.with(context)
                    .load(pull.getOwner().getAvatarUrl())
                    .apply(RequestOptions.circleCropTransform())
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(holder.imgProfile);
        } else {
            Glide.with(context).load(R.drawable.account_circle).into(holder.imgProfile);
        }
    }

    @Override
    public int getItemCount() {
        return pullRequests == null ? 0 : pullRequests.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtRepositoryName)
        TextView txtRepositoryName;

        @BindView(R.id.txtRepositoryDescription)
        TextView txtRepositoryDescription;

        @BindView(R.id.imgProfile)
        ImageView imgProfile;

        @BindView(R.id.txtLogin)
        TextView txtLogin;

        @BindView(R.id.txCreatedAt)
        TextView txCreatedAt;

        public ViewHolder(View view, final PullRequestAdapter adapter) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(v -> adapter.onItemHolderClick(PullRequestAdapter.ViewHolder.this));
        }
    }
}
