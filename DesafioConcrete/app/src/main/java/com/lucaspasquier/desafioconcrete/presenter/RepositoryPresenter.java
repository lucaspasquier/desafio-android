package com.lucaspasquier.desafioconcrete.presenter;

import java.io.Serializable;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public interface RepositoryPresenter {
    void onReceiveSerializable(Serializable serializableExtra);

    void onCreate(int i, int page);

    void onDestroy();

    void loadMoreItens(int page);
}
