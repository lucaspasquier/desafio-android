package com.lucaspasquier.desafioconcrete.di.component.activity;

import com.lucaspasquier.desafioconcrete.di.component.common.AppComponent;
import com.lucaspasquier.desafioconcrete.di.module.activity.SplashModule;
import com.lucaspasquier.desafioconcrete.di.scope.ActivityScope;
import com.lucaspasquier.desafioconcrete.ui.activity.SplashActivity;

import dagger.Component;

/**
 * Created by lucaspasquier on 29/10/17.
 */

@ActivityScope
@Component(dependencies = AppComponent.class,
        modules = SplashModule.class
)
public interface SplashComponent {
    void inject(SplashActivity activity);
}
