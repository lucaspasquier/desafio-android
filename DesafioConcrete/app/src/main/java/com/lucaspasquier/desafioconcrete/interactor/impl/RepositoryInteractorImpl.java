package com.lucaspasquier.desafioconcrete.interactor.impl;

import com.lucaspasquier.desafioconcrete.interactor.RepositoryInteractor;
import com.lucaspasquier.desafioconcrete.model.PullRequest;
import com.lucaspasquier.desafioconcrete.model.Repository;
import com.lucaspasquier.desafioconcrete.model.mapper.RepositoryMapper;
import com.lucaspasquier.desafioconcrete.service.Api;

import java.util.List;

import rx.Observable;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class RepositoryInteractorImpl implements RepositoryInteractor {

    private final Api api;
    private final RepositoryMapper mapper;

    public RepositoryInteractorImpl(Api api, RepositoryMapper mapper) {
        this.api = api;
        this.mapper = mapper;
    }

    @Override
    public Observable<List<Repository>> getRepositories(String language, String stars, int page) {
        return api.getRepositories(language, stars, page)
                .map(mapper::transform);
    }

    @Override
    public Observable<List<PullRequest>> getPullRequests(String owner, String repositoryName, String state) {
        return api.getPullRequests(owner, repositoryName, state)
                .map(mapper::transform);
    }
}
