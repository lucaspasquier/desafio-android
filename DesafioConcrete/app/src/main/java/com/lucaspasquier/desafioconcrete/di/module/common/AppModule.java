package com.lucaspasquier.desafioconcrete.di.module.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lucaspasquier.desafioconcrete.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lucaspasquier on 29/10/17.
 */

@Module
public class AppModule {

    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    App provideApp(){ return app; }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
    }
}
