package com.lucaspasquier.desafioconcrete.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.lucaspasquier.desafioconcrete.R;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class ProgressDialog extends Dialog {

    public ProgressDialog(Context context) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.setContentView(R.layout.dialog_progress);
        this.setCancelable(false);
    }
}