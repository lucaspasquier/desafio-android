package com.lucaspasquier.desafioconcrete.util;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class RxComposer {

    public static <T> Observable.Transformer<T, T> ioThread() {
        return observable -> observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
