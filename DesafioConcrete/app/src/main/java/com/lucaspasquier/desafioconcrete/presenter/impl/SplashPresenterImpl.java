package com.lucaspasquier.desafioconcrete.presenter.impl;

import android.os.Handler;

import com.lucaspasquier.desafioconcrete.presenter.SplashPresenter;
import com.lucaspasquier.desafioconcrete.ui.view.SplashView;
import com.lucaspasquier.desafioconcrete.util.RxComposer;

import java.util.concurrent.TimeUnit;

import rx.Observable;


/**
 * Created by lucaspasquier on 29/10/17.
 */

public class SplashPresenterImpl implements SplashPresenter {

    private static final long DELAY = 5000;

    private final SplashView view;

    public SplashPresenterImpl(SplashView view) {
        this.view = view;
    }

    @Override
    public void onCreate() {
        view.setUpVideoBackground();
        new Handler().postDelayed(() -> view.navigateToRepositories(), DELAY);
    }

    @Override
    public void onDestroy() {
    }
}

