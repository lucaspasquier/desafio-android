package com.lucaspasquier.desafioconcrete.di.module.activity;

import com.lucaspasquier.desafioconcrete.presenter.SplashPresenter;
import com.lucaspasquier.desafioconcrete.presenter.impl.SplashPresenterImpl;
import com.lucaspasquier.desafioconcrete.ui.view.SplashView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lucaspasquier on 29/10/17.
 */

@Module
public class SplashModule {

    private SplashView view;

    public SplashModule(SplashView view) {
        this.view = view;
    }

    @Provides
    public SplashView provideSplashView() {
        return this.view;
    }

    @Provides
    public SplashPresenter provideSplashPresenter(SplashView view) {
        return new SplashPresenterImpl(view);
    }
}
