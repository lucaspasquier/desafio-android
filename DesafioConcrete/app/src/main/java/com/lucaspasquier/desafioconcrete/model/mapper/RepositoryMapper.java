package com.lucaspasquier.desafioconcrete.model.mapper;

import com.lucaspasquier.desafioconcrete.model.PullRequest;
import com.lucaspasquier.desafioconcrete.model.Repository;
import com.lucaspasquier.desafioconcrete.model.api.APiRepository;
import com.lucaspasquier.desafioconcrete.model.api.APiRepositoryItem;
import com.lucaspasquier.desafioconcrete.model.api.ApiPullRequest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class RepositoryMapper {

    @Inject
    public RepositoryMapper() {}

    public List<Repository> transform(APiRepository aPiRepository) {
        List<Repository> repositories = new ArrayList<>();

        for (APiRepositoryItem item : aPiRepository.getItems()) {

            Repository repository = new Repository();

            repository.setId(item.getId());
            repository.setDescription(item.getDescription());
            repository.setForksCount(item.getForksCount());
            repository.setName(item.getName());
            repository.setStargazersCount(item.getStargazersNount());
            repository.setOwner(item.getOwner());
            repository.setFullname(item.getFullName());

            repositories.add(repository);
        }

        return repositories;
    }

    public List<PullRequest> transform(List<ApiPullRequest> apiPullRequests) {
        List<PullRequest> pullRequests = new ArrayList<>();

        for (ApiPullRequest apiPullRequest: apiPullRequests) {
            PullRequest pullRequest = new PullRequest();

            pullRequest.setOwner(apiPullRequest.getOwner());
            pullRequest.setDescription(apiPullRequest.getDescription());
            pullRequest.setTitle(apiPullRequest.getTitle());
            pullRequest.setUrl(apiPullRequest.getUrl());
            pullRequest.setState(apiPullRequest.getState());
            pullRequest.setCreatedAt(apiPullRequest.getCreatedAt());

            pullRequests.add(pullRequest);
        }

        return  pullRequests;
    }
}
