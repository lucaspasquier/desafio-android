package com.lucaspasquier.desafioconcrete.interactor;

import com.lucaspasquier.desafioconcrete.model.PullRequest;
import com.lucaspasquier.desafioconcrete.model.Repository;

import java.util.List;

import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public interface RepositoryInteractor {

    Observable<List<Repository>> getRepositories(String language,
                                                 String stars,
                                                 int page);

    Observable<List<PullRequest>> getPullRequests(String login, String repositoryName, String state);


}
