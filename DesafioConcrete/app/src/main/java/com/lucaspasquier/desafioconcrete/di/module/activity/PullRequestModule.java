package com.lucaspasquier.desafioconcrete.di.module.activity;

import com.lucaspasquier.desafioconcrete.interactor.RepositoryInteractor;
import com.lucaspasquier.desafioconcrete.presenter.PullResquestPresenter;
import com.lucaspasquier.desafioconcrete.presenter.RepositoryPresenter;
import com.lucaspasquier.desafioconcrete.presenter.impl.PullResquestPresenterImpl;
import com.lucaspasquier.desafioconcrete.presenter.impl.RepositoryPresenterImpl;
import com.lucaspasquier.desafioconcrete.ui.view.PullResquestView;
import com.lucaspasquier.desafioconcrete.ui.view.RepositoryView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lucaspasquier on 29/10/17.
 */

@Module
public class PullRequestModule {

    private PullResquestView view;

    public PullRequestModule(PullResquestView view) {
        this.view = view;
    }

    @Provides
    public PullResquestView providePullResquestView() {
        return this.view;
    }

    @Provides
    public PullResquestPresenter provideRepositoryPresenter(PullResquestView view, RepositoryInteractor interactor) {
        return new PullResquestPresenterImpl(view, interactor);
    }
}
