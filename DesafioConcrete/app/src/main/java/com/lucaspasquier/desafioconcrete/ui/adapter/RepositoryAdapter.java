package com.lucaspasquier.desafioconcrete.ui.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.lucaspasquier.desafioconcrete.R;
import com.lucaspasquier.desafioconcrete.model.Repository;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder>{

    private final Context context;
    private final Drawable icFork;
    private final Drawable icStar;

    private AdapterView.OnItemClickListener mOnItemClickListener;
    private List<Repository> repositories;

    public RepositoryAdapter(Context context, List<Repository> repositories) {
        this.context = context;
        this.repositories = repositories;

        this.icFork = ContextCompat.getDrawable(context, R.drawable.ic_source_branch);
        this.icFork.setColorFilter(ContextCompat.getColor(context, R.color.yellow_800), PorterDuff.Mode.SRC_ATOP);

        this.icStar = ContextCompat.getDrawable(context, R.drawable.ic_star);
        this.icStar.setColorFilter(ContextCompat.getColor(context, R.color.yellow_800), PorterDuff.Mode.SRC_ATOP);
    }


    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(RepositoryAdapter.ViewHolder itemHolder) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClick(null, itemHolder.itemView,
                    itemHolder.getAdapterPosition(), itemHolder.getItemId());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_repository_item, parent, false);
        return new ViewHolder(view, RepositoryAdapter.this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Repository repository = repositories.get(position);


        holder.txtRepositoryName.setText(repository.getName());
        holder.txtRepositoryDescription.setText(repository.getDescription());
        holder.txtFork.setText(String.valueOf(repository.getForksCount()));
        holder.txtStars.setText(String.valueOf(repository.getStargazersCount()));
        holder.txtLogin.setText(repository.getOwner().getLogin());
        holder.txName.setText(repository.getFullname());

        Glide.with(context).load(icFork).into(holder.imgStar);
        Glide.with(context).load(icStar).into(holder.imgStar);

        if(!repository.getOwner().getAvatarUrl().isEmpty()) {
            Glide.with(context)
                    .load(repository.getOwner().getAvatarUrl())
                    .apply(RequestOptions.circleCropTransform())
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(holder.imgProfile);
        } else {
            Glide.with(context).load(R.drawable.account_circle).into(holder.imgProfile);
        }
    }

    @Override
    public int getItemCount() {
        return repositories == null ? 0 : repositories.size();
    }

    public void notifyDataSetChanged(List<Repository> repositories) {
        this.repositories.addAll(repositories);
        this.notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtRepositoryName)
        TextView txtRepositoryName;

        @BindView(R.id.txtRepositoryDescription)
        TextView txtRepositoryDescription;

        @BindView(R.id.imgFork)
        ImageView imgFork;

        @BindView(R.id.txtFork)
        TextView txtFork;

        @BindView(R.id.imgStar)
        ImageView imgStar;

        @BindView(R.id.txtStars)
        TextView txtStars;

        @BindView(R.id.imgProfile)
        ImageView imgProfile;

        @BindView(R.id.txtLogin)
        TextView txtLogin;

        @BindView(R.id.txName)
        TextView txName;

        public ViewHolder(View view, final RepositoryAdapter adapter) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(v -> adapter.onItemHolderClick(RepositoryAdapter.ViewHolder.this));
        }
    }
}
