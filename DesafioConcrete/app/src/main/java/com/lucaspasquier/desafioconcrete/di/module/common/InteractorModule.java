package com.lucaspasquier.desafioconcrete.di.module.common;

import com.lucaspasquier.desafioconcrete.interactor.RepositoryInteractor;
import com.lucaspasquier.desafioconcrete.interactor.impl.RepositoryInteractorImpl;
import com.lucaspasquier.desafioconcrete.model.mapper.RepositoryMapper;
import com.lucaspasquier.desafioconcrete.service.Api;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lucaspasquier on 29/10/17.
 */

@Module
public class InteractorModule {

    @Provides
    public RepositoryInteractor provideRepositoryInteractor(Api api, RepositoryMapper mapper) {
        return new RepositoryInteractorImpl(api, mapper);
    }
}
