package com.lucaspasquier.desafioconcrete.di.module.common;

import com.google.gson.Gson;
import com.lucaspasquier.desafioconcrete.BuildConfig;
import com.lucaspasquier.desafioconcrete.service.Api;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by lucaspasquier on 29/10/17.
 */

@Module
public class ApiModule {

    public ApiModule() {
    }

    @Provides
    @Singleton
    public Api provideApplicationService(Converter.Factory factory, OkHttpClient client) {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(factory)
                .baseUrl(BuildConfig.API_URL)
                .client(client)
                .build();

        return retrofit.create(Api.class);
    }

    @Provides
    public Converter.Factory provideConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    public OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();
    }
}
