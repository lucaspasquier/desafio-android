package com.lucaspasquier.desafioconcrete.ui.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lucaspasquier.desafioconcrete.R;
import com.lucaspasquier.desafioconcrete.di.component.activity.DaggerPullRequestComponent;
import com.lucaspasquier.desafioconcrete.di.component.common.AppComponent;
import com.lucaspasquier.desafioconcrete.di.module.activity.PullRequestModule;
import com.lucaspasquier.desafioconcrete.model.PullRequest;
import com.lucaspasquier.desafioconcrete.model.Repository;
import com.lucaspasquier.desafioconcrete.presenter.PullResquestPresenter;
import com.lucaspasquier.desafioconcrete.ui.adapter.PullRequestAdapter;
import com.lucaspasquier.desafioconcrete.ui.view.PullResquestView;
import com.lucaspasquier.desafioconcrete.util.Util;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import jp.wasabeef.recyclerview.animators.SlideInDownAnimator;

/**
 * Created by lucaspasquier on 30/10/17.
 */

public class PullResquestActivity extends BaseActivity implements PullResquestView {

    private static final String SCROLL_POSITION = "SCROLL_POSITION";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.txtPulls)
    TextView txtPulls;

    @BindView(R.id.rcvPullRequests)
    RecyclerView rcvPullRequests;

    @BindView(R.id.lltEmptyResult)
    LinearLayout lltEmptyResult;

    @BindView(R.id.txtDescription)
    TextView txtDescription;

    @Inject
    PullResquestPresenter presenter;

    private Unbinder unbinder;
    private List<PullRequest> pullRequests;
    private LinearLayoutManager layoutManager;

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerPullRequestComponent.builder()
                .appComponent(appComponent)
                .pullRequestModule(new PullRequestModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);
        unbinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(savedInstanceState != null && !savedInstanceState.isEmpty()) {
            presenter.onReceiveRepository(savedInstanceState.getSerializable(Repository.ID_OBJECT));
            presenter.onReceivePullRequests(savedInstanceState.getSerializable(PullRequest.ID_OBJECT));
            presenter.onReceiveInt(savedInstanceState.getInt(SCROLL_POSITION));
        } else if(getIntent().hasExtra(Repository.ID_OBJECT)) {
            presenter.onReceiveSerializable(getIntent().getSerializableExtra(Repository.ID_OBJECT));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(PullRequest.ID_OBJECT, (Serializable) pullRequests);
        outState.putSerializable(Repository.ID_OBJECT, presenter.getRepository());
        outState.putInt(SCROLL_POSITION, layoutManager.findFirstVisibleItemPosition());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        presenter.onDestroy();
    }

    @Override
    public void showProgress() {
        super.showProgress();
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
    }

    @Override
    public void loadResult(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
        PullRequestAdapter adapter = new PullRequestAdapter(PullResquestActivity.this, pullRequests);
        layoutManager = new LinearLayoutManager(PullResquestActivity.this, LinearLayoutManager.VERTICAL, false);
        int open = 0;
        int closed = 0;
        String text = "";

        rcvPullRequests.setLayoutManager(layoutManager);
        rcvPullRequests.setItemAnimator(new SlideInDownAnimator());
        rcvPullRequests.setAdapter(adapter);

        adapter.setOnItemClickListener((parent, view, position, id) -> {

            try {
                PullRequest pullRequest = pullRequests.get(position);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(pullRequest.getUrl()));
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(this, getString(R.string.no_browser),  Toast.LENGTH_LONG).show();
            }

        });

        for (PullRequest pullRequest : pullRequests) {

            if(PullRequest.OPEN_STATE.equalsIgnoreCase(pullRequest.getState())) {
                open++;
            } else if(PullRequest.CLOSED_STATE.equalsIgnoreCase(pullRequest.getState())) {
                closed++;
            }
        }

        text = String.format(getString(R.string.pull_requests_state), open, closed);

        Spannable messageSpannable = new SpannableString(text);
        messageSpannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.yellow_800)),
                0, Util.getPosition(txtPulls.getText().toString(), '/'), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        txtPulls.setText(messageSpannable);
    }

    @Override
    public void loadErrorScreen() {
        lltEmptyResult.setVisibility(View.VISIBLE);
        txtDescription.setText(getString(R.string.unexpected_error));
    }

    @Override
    public void loadEmptyResult() {
        lltEmptyResult.setVisibility(View.VISIBLE);
        txtDescription.setText(getString(R.string.empty_result));
    }

    @Override
    public void loadRepositoryInfo(Repository reposiroty) {
        toolbar.setTitle(reposiroty.getName());
        getSupportActionBar().setTitle(reposiroty.getName());
        setTitle(reposiroty.getName());
    }

    @Override
    public void scrollToPosition(int position) {
        rcvPullRequests.scrollToPosition(position);
    }
}
