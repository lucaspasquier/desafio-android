package com.lucaspasquier.desafioconcrete.presenter;

import com.lucaspasquier.desafioconcrete.model.Repository;

import java.io.Serializable;

/**
 * Created by lucaspasquier on 30/10/17.
 */

public interface PullResquestPresenter {

    void onReceiveSerializable(Serializable serializableExtra);

    void onDestroy();

    Repository getRepository();

    void onReceiveRepository(Serializable serializable);

    void onReceivePullRequests(Serializable serializable);

    void onReceiveInt(int position);
}
