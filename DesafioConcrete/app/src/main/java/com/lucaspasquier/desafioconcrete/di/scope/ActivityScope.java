package com.lucaspasquier.desafioconcrete.di.scope;

/**
 * Created by lucaspasquier on 29/10/17.
 */
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityScope {}