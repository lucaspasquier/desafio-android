package com.lucaspasquier.desafioconcrete;

import android.app.Application;
import android.content.Context;

import com.lucaspasquier.desafioconcrete.di.component.common.AppComponent;
import com.lucaspasquier.desafioconcrete.di.component.common.DaggerAppComponent;
import com.lucaspasquier.desafioconcrete.di.module.common.AppModule;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class App extends Application {

    private static Context context;
    private static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        initComponent();
    }

    private void initComponent() {
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        component.inject(this);
    }

    public static AppComponent getComponent() {
        return component;
    }

    public static Context getContext() {
        return context;
    }
}
