package com.lucaspasquier.desafioconcrete.presenter.impl;

import android.util.Log;

import com.lucaspasquier.desafioconcrete.interactor.RepositoryInteractor;
import com.lucaspasquier.desafioconcrete.model.PullRequest;
import com.lucaspasquier.desafioconcrete.model.Repository;
import com.lucaspasquier.desafioconcrete.presenter.PullResquestPresenter;
import com.lucaspasquier.desafioconcrete.ui.view.PullResquestView;
import com.lucaspasquier.desafioconcrete.util.RxComposer;
import com.lucaspasquier.desafioconcrete.util.Util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rx.Subscription;

/**
 * Created by lucaspasquier on 30/10/17.
 */

public class PullResquestPresenterImpl implements PullResquestPresenter {

    private static final String STATE = "all";

    private final PullResquestView view;
    private final RepositoryInteractor interactor;
    private Subscription subscription;

    private Repository reposiroty;

    public PullResquestPresenterImpl(PullResquestView view, RepositoryInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void onReceiveSerializable(Serializable serializableExtra) {
        if(serializableExtra instanceof Repository) {
            this.reposiroty = (Repository) serializableExtra;

            view.loadRepositoryInfo(reposiroty);

            this.search();
        }

    }

    @Override
    public void onDestroy() {
        if(subscription != null && subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public Repository getRepository() {
        return this.reposiroty;
    }

    @Override
    public void onReceiveRepository(Serializable serializable) {
        if(serializable instanceof Repository) {
            reposiroty = (Repository) serializable;
        }
    }

    @Override
    public void onReceivePullRequests(Serializable serializable) {
        if(serializable instanceof ArrayList) {
            loadResults((List<PullRequest>) serializable);
        }
    }

    @Override
    public void onReceiveInt(int position) {
        view.scrollToPosition(position);
    }

    private void search() {
        subscription = interactor
                .getPullRequests(reposiroty.getOwner().getLogin(), reposiroty.getName(), STATE)
                .compose(RxComposer.ioThread())
                .doOnSubscribe(view::showProgress)
                .doOnCompleted(view::hideProgress)
                .doOnError(e -> {
                    Log.e("Error", e.getMessage());
                    view.hideProgress();
                    view.loadErrorScreen();
                })
                .subscribe(pullRequests -> loadResults(pullRequests),
                        throwable -> view.loadErrorScreen());
    }

    private void loadResults(List<PullRequest> pullRequests) {
        if(!Util.isEmpty(pullRequests)) {
            view.loadResult(pullRequests);
        } else {
            view.loadEmptyResult();
        }
    }
}
