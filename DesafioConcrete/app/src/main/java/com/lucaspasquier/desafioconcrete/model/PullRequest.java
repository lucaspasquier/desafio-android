package com.lucaspasquier.desafioconcrete.model;

import com.lucaspasquier.desafioconcrete.model.api.ApiOwner;
import com.lucaspasquier.desafioconcrete.util.Util;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class PullRequest implements Serializable {

    public static final String ID_OBJECT = "com.lucaspasquier.desafioconcrete.model.PullRequest";

    public static final String OPEN_STATE = "open";
    public static final String CLOSED_STATE = "closed";

    private String title;

    private String description;

    private Owner owner;

    private String url;

    private String state;

    private Date createdAt;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if(title == null) {
            title = "";
        }

        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if(description == null) {
            description = "";
        }

        this.description = description;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(ApiOwner owner) {

        if(owner == null) {
            this.owner = new Owner();
        } else {
            this.owner = new Owner(owner);
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {

        if(url == null) {
            url = "";
        }

        this.url = url;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {

        if(state == null) {
            state = "";
        }

        this.state = state;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAtToString() {

        if(createdAt != null) {
            return Util.getDateAsString(createdAt);
        }

        return "";
    }
}
