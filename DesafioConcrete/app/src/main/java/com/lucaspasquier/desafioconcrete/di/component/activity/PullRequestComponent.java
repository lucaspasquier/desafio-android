package com.lucaspasquier.desafioconcrete.di.component.activity;

import com.lucaspasquier.desafioconcrete.di.component.common.AppComponent;
import com.lucaspasquier.desafioconcrete.di.module.activity.PullRequestModule;
import com.lucaspasquier.desafioconcrete.di.module.activity.RepositoryModule;
import com.lucaspasquier.desafioconcrete.di.scope.ActivityScope;
import com.lucaspasquier.desafioconcrete.ui.activity.PullResquestActivity;
import com.lucaspasquier.desafioconcrete.ui.activity.RepositoryActivity;

import dagger.Component;

/**
 * Created by lucaspasquier on 29/10/17.
 */

@ActivityScope
@Component(dependencies = AppComponent.class,
        modules = PullRequestModule.class
)
public interface PullRequestComponent {
    void inject(PullResquestActivity activity);
}
