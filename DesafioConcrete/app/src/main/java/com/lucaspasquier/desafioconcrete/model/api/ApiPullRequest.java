package com.lucaspasquier.desafioconcrete.model.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class ApiPullRequest implements Serializable {

    @SerializedName("title")
    private String title;

    @SerializedName("body")
    private String description;

    @SerializedName("created_at")
    private Date createdAt;

    @SerializedName("user")
    private ApiOwner owner;

    @SerializedName("html_url")
    private String url;

    @SerializedName("state")
    private String state;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public ApiOwner getOwner() {
        return owner;
    }

    public String getUrl() {
        return url;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getState() {
        return state;
    }
}
