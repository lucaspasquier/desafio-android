package com.lucaspasquier.desafioconcrete.presenter.impl;

import android.util.Log;

import com.lucaspasquier.desafioconcrete.interactor.RepositoryInteractor;
import com.lucaspasquier.desafioconcrete.model.Repository;
import com.lucaspasquier.desafioconcrete.presenter.RepositoryPresenter;
import com.lucaspasquier.desafioconcrete.ui.view.RepositoryView;
import com.lucaspasquier.desafioconcrete.util.RxComposer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rx.Subscription;

/**
 * Created by lucaspasquier on 29/10/17.
 */

public class RepositoryPresenterImpl implements RepositoryPresenter {

    private static final String DEFAULT_LANGUAGE = "Java";
    private static final String DEFAULT_SORT = "stars";

    private final RepositoryView view;
    private final RepositoryInteractor interactor;

    private List<Repository> repository;
    private Subscription subscription;

    public RepositoryPresenterImpl(RepositoryView view, RepositoryInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void onReceiveSerializable(Serializable serializableExtra) {
        if (serializableExtra instanceof ArrayList) {
            this.repository = (List<Repository>) serializableExtra;
        }
    }

    @Override
    public void onCreate(int pageNumber, int position) {
        if (repository == null) {
            search(pageNumber);
        } else {
            view.loadResult(repository);

            if(position > 0) {
                view.scrollPosition(position);
            }
        }
    }

    @Override
    public void loadMoreItens(int page) {
        search(page);
    }

    private void search(int pageNumber) {
        subscription = interactor.getRepositories(DEFAULT_LANGUAGE, DEFAULT_SORT, pageNumber)
                .compose(RxComposer.ioThread())
                .doOnSubscribe(view::showProgress)
                .doOnCompleted(view::hideProgress)
                .doOnError(e -> {
                    Log.e("Error", e.getMessage());
                    view.hideProgress();
                    view.loadErrorScreen();
                })
                .subscribe(repositories -> loadResults(repositories, pageNumber),
                        throwable -> view.loadErrorScreen());
    }

    private void loadResults(List<Repository> repositories, int pageNumber) {
        if (!repositories.isEmpty() && pageNumber == 0) {
            view.loadResult(repositories);
        } else if (pageNumber != 0) {
            view.loadPagination(repositories);
        } else {
            view.loadEmptyResult();
        }
    }

    @Override
    public void onDestroy() {
        if (subscription != null && subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
