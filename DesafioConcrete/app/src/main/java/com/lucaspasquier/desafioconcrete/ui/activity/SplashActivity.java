package com.lucaspasquier.desafioconcrete.ui.activity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.VideoView;

import com.lucaspasquier.desafioconcrete.R;
import com.lucaspasquier.desafioconcrete.di.component.activity.DaggerSplashComponent;
import com.lucaspasquier.desafioconcrete.di.component.common.AppComponent;
import com.lucaspasquier.desafioconcrete.di.module.activity.SplashModule;
import com.lucaspasquier.desafioconcrete.presenter.SplashPresenter;
import com.lucaspasquier.desafioconcrete.ui.view.SplashView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SplashActivity extends BaseActivity implements SplashView {

    @BindView(R.id.video)
    VideoView video;

    @Inject
    SplashPresenter presenter;

    private Unbinder unbinder;

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerSplashComponent.builder()
                .appComponent(appComponent)
                .splashModule(new SplashModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        unbinder = ButterKnife.bind(this);

        presenter.onCreate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();

        if(unbinder != null) {
            unbinder.unbind();
        }

//        video.stop
    }

    @Override
    public void navigateToRepositories() {
        startActivity(new Intent(SplashActivity.this, RepositoryActivity.class));
        this.finish();
    }

    @Override
    public void setUpVideoBackground() {
        String path = "android.resource://" + getPackageName() + "/" + R.raw.concrete_logo;
        video.setVideoURI(Uri.parse(path));
        video.setMediaController(null);

        video.start();
    }
}
